
$(document).ready(function () {
    $('.parallax-wrapper').mousemove(function(e){
	
        var x = e.pageX - $(this).offset().left - ($(this).width()/2)
        var y = e.pageY - $(this).offset().top - ($(this).height()/2)
        
        $('*[data-mouse-parallax]').each(function(){
            var factor = parseFloat($(this).data('mouse-parallax'))
            x = x * factor; y = y * factor
    
            $(this).css({ 'transform': 'translate3d( '+ x + 'px, ' + y + 'px, 0 )' })
        })
        
    })
    
    
    
});

function timer() {
    var time_now = new Date();
    time_now = Date.parse(time_now) / 1000;
    var endtime = new Date("21 November 2021 00:00:00");
    endtime = Date.parse(endtime) / 1000;
    var time_left = endtime - time_now;
    var days = Math.floor(time_left / 86400);
    var hours = Math.floor((time_left - days * 86400) / 3600);
    var minutes = Math.floor((time_left - days * 86400 - hours * 3600) / 60);
    var seconds = Math.floor(time_left - days * 86400 - hours * 3600 - minutes * 60);

    if (hours < "10") {
        hours = "0" + hours;
    }
    if (minutes < "10") {
        minutes = "0" + minutes;
    }
    if (seconds < "10") {
        seconds = "0" + seconds;
    }
    if (time_left <= 0) {
        days = '00';hours = '00';minutes = '00';seconds = '00';
    }
    $(".days .time").html(days);
    $(".hours .time").html(hours);
    $(".minutes .time").html(minutes);
    $(".seconds .time").html(seconds);
}
setInterval(function () {
    timer();
}, 1000);
